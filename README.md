# 安卓USB串口通信

#### 项目介绍
站在大佬的肩膀上！

https://blog.csdn.net/qq_16064871/article/details/77987681 （感谢原作者）

如有侵权，立即删除！

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1.- 列举USB串口设备+连接设备
```java
// 列举可用的USB设备
UsbSerialPortUtil.listSerialPort(this, new OnListedSerialPortListener() {
    @Override
    public void onFinished(List<UsbSerialPort> result) {
        Toast.makeText(MainActivity.this, result.size() + "", Toast.LENGTH_SHORT).show();
        if (result.size() == 1) {
            // 随便抓一个设备拿来用
            connection = UsbSerialPortUtil.connect(MainActivity.this, result.get(0), new OnConnectedCallback() {
                // 这里用了回调，因为不确定该程序是否有对该USB设备的控制权。有权限或请求的权限通过后，则调用回调内方法。
                @Override
                public void onConnected(Connection connection) {
                    connection.setListener(MainActivity.this)
                            .open()
                            .autoClose()
                            .setParamters(9600, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);
                }

                @Override
                public void onGrantFailed(Context context, UsbSerialPort usbSerialPort) {
                    Toast.makeText(MainActivity.this, "Grant failed", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
});
```
- 同步写
```
connection.writeSync("ABC".getBytes());
```
- 异步写
```
connection.writeAsync("ABC".getBytes());
```
- 回调
```
public interface OnConnectionMessageListener {
    void onError(Connection connection, Exception e);
    void onData(Connection connection, byte[] data);
    void onClosed(Connection connection);
    void onOpened(Connection connection);
}
```

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)