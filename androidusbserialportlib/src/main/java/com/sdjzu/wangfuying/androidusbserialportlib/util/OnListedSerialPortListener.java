package com.sdjzu.wangfuying.androidusbserialportlib.util;
/*
作者：王福迎
邮箱：<805447391@qq.com>
网址：https://gitee.com/wangfuying_admin
*/

import com.hoho.android.usbserial.driver.UsbSerialPort;

import java.util.List;

public interface OnListedSerialPortListener {
    void onFinished(List<UsbSerialPort> result);
}
