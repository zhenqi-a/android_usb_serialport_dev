package com.sdjzu.wangfuying.androidusbserialportlib.util;
/*
作者：王福迎
邮箱：<805447391@qq.com>
网址：https://gitee.com/wangfuying_admin
*/

import android.content.Context;

import com.hoho.android.usbserial.driver.UsbSerialPort;

public interface OnConnectedCallback {
    void onConnected(Connection connection);

    void onGrantFailed(Context context, UsbSerialPort usbSerialPort);
}
