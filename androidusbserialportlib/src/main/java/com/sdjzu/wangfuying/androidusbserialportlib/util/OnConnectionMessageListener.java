package com.sdjzu.wangfuying.androidusbserialportlib.util;
/*
作者：王福迎
邮箱：<805447391@qq.com>
网址：https://gitee.com/wangfuying_admin
*/

public interface OnConnectionMessageListener {
    void onError(Connection connection, Exception e);

    void onData(Connection connection, byte[] data);

    void onClosed(Connection connection);

    void onOpened(Connection connection);
}
