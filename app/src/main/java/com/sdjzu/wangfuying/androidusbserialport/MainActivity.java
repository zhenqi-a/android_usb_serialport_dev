package com.sdjzu.wangfuying.androidusbserialport;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sdjzu.wangfuying.androidusbserialportlib.util.Connection;
import com.sdjzu.wangfuying.androidusbserialportlib.util.OnConnectedCallback;
import com.sdjzu.wangfuying.androidusbserialportlib.util.OnConnectionMessageListener;
import com.sdjzu.wangfuying.androidusbserialportlib.util.OnListedSerialPortListener;
import com.sdjzu.wangfuying.androidusbserialportlib.util.UsbSerialPortUtil;
import com.hoho.android.usbserial.driver.UsbSerialPort;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnConnectionMessageListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private List<UsbSerialPort> mEntries = new ArrayList<UsbSerialPort>();
    private Connection connection;
    private TextView txt_hello = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn_send = findViewById(R.id.button);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != connection) {
                    connection.writeSync("ABC".getBytes());
                }
            }
        });
        txt_hello = findViewById(R.id.activity_main_txt_hello);

        // 列举可用的USB设备
        UsbSerialPortUtil.listSerialPort(this, new OnListedSerialPortListener() {
            @Override
            public void onFinished(List<UsbSerialPort> result) {
                Toast.makeText(MainActivity.this, result.size() + "", Toast.LENGTH_SHORT).show();
                if (result.size() == 1) {
                    // 随便抓一个设备拿来用
                    connection = UsbSerialPortUtil.connect(MainActivity.this, result.get(0), new OnConnectedCallback() {
                        // 这里用了回调，因为不确定该程序是否有对该USB设备的控制权。有权限或请求的权限通过后，则调用回调内方法。
                        @Override
                        public void onConnected(Connection connection) {
                            connection.setListener(MainActivity.this)
                                    .open()
                                    .autoClose()
                                    .setParamters(9600, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);
                        }

                        @Override
                        public void onGrantFailed(Context context, UsbSerialPort usbSerialPort) {
                            Toast.makeText(MainActivity.this, "Grant failed", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onError(Connection connection, Exception e) {
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onData(Connection connection, byte[] data) {
        // Toast.makeText(this, "onData: " + new String(data, Charset.forName("utf-8")), Toast.LENGTH_SHORT).show();
        txt_hello.setText(new String(data, Charset.forName("utf-8")));
        Toast.makeText(this, data.length + "", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClosed(Connection connection) {
        Toast.makeText(this, "onClosed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onOpened(Connection connection) {
        Toast.makeText(this, "onOpened", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*if (null != connection)
            connection.close();*/
    }
}
